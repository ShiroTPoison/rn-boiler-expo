import getDashboardDataReducer from "../../../screens/dashboard/reducers/getDashboardDataReducer";

const reducers = {
  getDashboardDataReducer,
};
export default reducers;
