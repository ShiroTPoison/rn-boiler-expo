import appAppearanceReducer from './setAppApearanceReducer';
import locationReducer from './locationReducer';
import addressReducer from './addressReducer';
import languageReducer from './languageReducer';
import gDateReducer from './globalDateReducer';

const reducers = {
  appAppearanceReducer,
  addressReducer,
  locationReducer,
  languageReducer,
  gDateReducer,
};
export default reducers;
